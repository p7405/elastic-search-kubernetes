#Subir un proyecto nuevo
Add your files

~~~
cd existing_repo
git remote add origin https://gitlab.com/p7405/elastic-search-kubernetes.git
git branch -M main
git push -uf origin main
~~~

# Download Elastic Cloud on Kubernetes
## INSTALACION DE ECK
1. para una primera instalacion, corra el siguiente comando

~~~ 
kubectl create -f https://download.elastic.co/downloads/eck/2.11.1/crds.yaml 
~~~

o si ya descargo el archivo puede ejecutar

~~~ 
kubectl create -f crds.yaml
~~~

2. ejecute el segundo comando

~~~
kubectl apply -f https://download.elastic.co/downloads/eck/2.11.1/operator.yaml
~~~

o si ya descargo el archivo puede ejecutar

~~~
kubectl apply -f operator.yaml
~~~

3. Supervice los Logs (registros del archivo operador)

~~~
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
~~~

## Desplegando un cluster Elastic

Para crear el despliegue de nuestro primer elasticsearch puedo crear el archivo elasticsearch.yml

~~~~
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: quickstart
spec:
  version: 8.12.2
  nodeSets:
  - name: default
    count: 1
    config:
      node.store.allow_mmap: false
~~~~
El operador crea y administra automáticamente los recursos de Kubernetes para lograr el estado deseado del clúster de Elasticsearch. Pueden pasar algunos minutos hasta que se creen todos los recursos y el clúster esté listo para su uso.

La configuración node.store.allow_mmap: falsetiene implicaciones de rendimiento y debe ajustarse para cargas de trabajo de producción como se describe en la sección Memoria virtual .

Ejectutamos el comando:

~~~
kubectl apply -f elasticsearch.yml
~~~

## Supervisar el estado del cluster y progreso de creacion
para optener una descripcion general del cluster de elasticsearch su estado y cantidad de nodos se usa el comando:

~~~
kubectl get elasticsearch
~~~
Para ver los pods que se encuentran en proceso

~~~
kubectl get pods --selector='elasticsearch.k8s.elastic.co/cluster-name=quickstart'
~~~